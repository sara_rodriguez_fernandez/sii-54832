#include <sys/types.h>;
#include <sys/stat.h>;
#include <fcntl.h>;
#include <stdio.h>;
#include <stdlib.h>;
#include <unistd.h>;
#include <string>;
#include <iostream>;

int main()
{
    int fd;
    int tube;
    char buf[2] ;
    
    //FIFO
    tube=mkfifo("/tmp/FIFO", 0666);
    if (tube<0) {
        perror("No puede crearse ");
        return 1;
    }
    
    // Abrir
    fd=open("/tmp/FIFO", O_RDONLY);
    if (fd<0) {
        perror("No puede abrirse ");
        return 1;
    }
    //bucle lectura
    
      while (1) 
    {
    if (read(fd,buf,2)<2){
         perror("Error leyendo");
        return 1;}
        
    write(1,"Jugador ",sizeof("Jugador "));
    write(1,&buf[0],1);
    write(1," marca 1 punto, lleva un total de ",sizeof(" marca 1 punto, lleva un total de "));
    write(1,&buf[1],1);
    write(1," puntos.\n",sizeof("puntos.\n"));
    }
    
    
    
    tube=close(fd);
    if(tube<0)
    {
        perror("Error cerrando");
        return 1;
    }
    tube=unlink("/tmp/FIFO");
    if(tube<0)
    {
        perror("Error borrando");
        return 1;
    }
    return 0;
}
