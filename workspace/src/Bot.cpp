#include "DatosMemoriaCompartida.h";
#include "Vector2D.h";
#include "Plano.h";
#include <sys/types.h>;
#include <sys/stat.h>;
#include <sys/mman.h>;
#include <fcntl.h>;
#include <stdio.h>;
#include <stdlib.h>;
#include <unistd.h>;
#include <fstream>;
#include "glut.h";
int main()
{
  int fd;
  DatosMemoriaCompartida *punteroDatos;
  fd = open("/tmp/datos.dat", O_RDWR); //abrimos el fichero de
  if (fd < 0)
  {
   perror ("Error al abrir el fichero");
   return 1;
  }
  
  punteroDatos = static_cast<DatosMemoriaCompartida*>(mmap (NULL,sizeof(DatosMemoriaCompartida), PROT_WRITE | PROT_READ, MAP_SHARED, fd,0)); //proyección
//comprobar error 
close(fd);
if (punteroDatos == MAP_FAILED){
      perror ("error en la proyección de fichero");
      exit(1);
    } 
      
   else
    {
        float centroraqueta=(punteroDatos->raqueta1.y1+punteroDatos->raqueta1.y2)/2;
        if(punteroDatos->esfera.centro.y < centroraqueta){
            punteroDatos->accion=-1;
        }
        else if(punteroDatos->esfera.centro.y==centroraqueta){
            punteroDatos->accion=0;
        }
        else if(punteroDatos->esfera.centro.y>centroraqueta){
            punteroDatos->accion=1;
        }   
    usleep(25000);
    
    }
    munmap(punteroDatos, sizeof(DatosMemoriaCompartida));
    unlink("/tmp/datos.dat");
     return 1;
}
  
