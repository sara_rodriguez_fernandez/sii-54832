//El autor de este codigo es Sara Rodriguez Fernandez
// Mundo.cpp: implementation of the CMundo class.
//
#include <fstream>
#include "MundoCliente.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>


CMundo::CMundo()
{
    Init();
}

CMundo::~CMundo()
{
   
    munmap(pdatos, sizeof(DatosMemoriaCompartida));
    unlink("/tmp/datos.dat");
    
    /*close(fdsc);
unlink("/tmp/FIFO_SC");

close(fdteclas);
unlink("/tmp/FIFO_TECLAS");*/
    
    sock_cs.Close();
    exit(0);
}

void CMundo::InitGL()
{
    //Habilitamos las luces, la renderizacion y el color de los materiales
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    
    glMatrixMode(GL_PROJECTION);
    gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
    glDisable (GL_LIGHTING);

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glColor3f(r,g,b);
    glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
    int len = strlen (mensaje );
    for (int i = 0; i < len; i++) 
        glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
        
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
    //Borrado de la pantalla    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Para definir el punto de vista
    glMatrixMode(GL_MODELVIEW); 
    glLoadIdentity();
    
    gluLookAt(0.0, 0, 17,  // posicion del ojo
        0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
        0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

    /////////////////
    ///////////
    //      AQUI EMPIEZA MI DIBUJO
    char cad[100];
    sprintf(cad,"Jugador1: %d",puntos1);
    print(cad,10,0,1,1,1);
    sprintf(cad,"Jugador2: %d",puntos2);
    print(cad,650,0,1,1,1);
    int i;
    for(i=0;i<paredes.size();i++)
        paredes[i].Dibuja();

    fondo_izq.Dibuja();
    fondo_dcho.Dibuja();
    jugador1.Dibuja();
    jugador2.Dibuja();
    esfera.Dibuja();

    /////////////////
    ///////////
    //      AQUI TERMINA MI DIBUJO
    ////////////////////////////

    //Al final, cambiar el buffer
    glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{   
    //Actualizar datos servidor
    char buf_sc[200];
    /*int err=read(fdsc,buf_sc,sizeof(buf_sc));
	if (err<sizeof(buf_sc))
	{
	perror("Error al leer fifo s-c");
	}*/
	
    int errsock=sock_cs.Receive(buf_sc, sizeof(buf_sc));
    if (errsock < sizeof(buf_sc)){
        perror("Error al leer fifo s-c");
        exit(1);
    }

    sscanf(buf_sc,"%f %f %f %f %f %f %f %f %f %f %f %d %d", &esfera.centro.x,&esfera.centro.y,&esfera.radio,&jugador1.x1,&jugador1.y1,&jugador1.x2,&jugador1.y2,&jugador2.x1,&jugador2.y1,&jugador2.x2,&jugador2.y2, &puntos1,&puntos2);

    //Fin del juego
   if((puntos1==3)||(puntos2==3)) {
	this->CMundo::~CMundo();
	}
    
    //Avance del juego
    jugador1.Mueve(0.025f);
    jugador2.Mueve(0.025f);
    esfera.Mueve(0.025f);

    
    //Mover raqueta automatica
    if(pdatos->accion==1) OnKeyboardDown('w',0,0);
    if(pdatos->accion==-1) OnKeyboardDown('s',0,0);
    
    //Actualizar datos bot
    pdatos->raqueta1=jugador1;
    pdatos->esfera=esfera;
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y)
{
    char buf_cs[10];
    sprintf(buf_cs, "%c", key);
    /*int err=write(fdteclas,buf_cs,sizeof(buf_cs));
	if (err<sizeof(buf_cs))
	{
	perror("Error escribir fifo teclas");
	exit(1);
	}*/
    int errtec=sock_cs.Send(buf_cs, sizeof(buf_cs));
    if(errtec < sizeof(buf_cs)){
        perror("Error escribir fifo teclas");
        exit(1);
    }
    if(key=='f') this->~CMundo();
}

void CMundo::Init()
{

/*fifo servidor-cliente (coordenadas)
int error=mkfifo("/tmp/FIFO_SC",0777);
if (error<0){
      perror ("error al crear fifo servidor-cliente");
    }
fdsc= open ("/tmp/FIFO_SC", O_RDONLY);
if (fdsc<0){
perror ("error al abrir fifo servidor-cliente");
}

//fifo envio teclas
int errort=mkfifo("/tmp/FIFO_TECLAS",0777);
if (errort<0){
      perror ("error al crear fifo teclas");
    }
fdteclas= open ("/tmp/FIFO_TECLAS",O_WRONLY);
if (fdteclas<0){
perror ("error al abrir fifo teclas");
}*/
    //Nombre usuario
    char nombre[200];
    write(1, "Introduzca su nombre: ", strlen("Introduzca su nombre: "));
    if(read(0, nombre, sizeof(nombre)) < 0){
        perror("Error lectura nombre");
        exit(1);
    }
    
    //Socket CS
    if((sock_cs.Connect("127.0.0.1", 25565)) < 0){
        perror("Error conexion cliente");
        exit(1);
    }
    if(sock_cs.Send(nombre, strlen(nombre)) < 0){
        perror("Error envio nombre");
        exit(1);
    }

    
    //Archivo bot
    datos.raqueta1=jugador1;
    datos.esfera=esfera;
    datos.accion=0;
    
    fd_datos=open("/tmp/datos.txt", O_RDWR|O_CREAT|O_TRUNC, 0666);
    if (fd_datos < 0){
        perror("Error fichero datos");
        exit(1);
    }
    write(fd_datos, &datos, sizeof(DatosMemoriaCompartida));
    
    pdatos = static_cast<DatosMemoriaCompartida*>(mmap (NULL, sizeof(datos), PROT_WRITE | PROT_READ, MAP_SHARED, fd_datos, 0)); //proyección al puntero
    if (pdatos == MAP_FAILED){
      perror ("error en la proyección de fichero");
      exit(1);
    }
    
    close(fd_datos);
    
    
    Plano p;
//pared inferior
    p.x1=-7;p.y1=-5;
    p.x2=7;p.y2=-5;
    paredes.push_back(p);

//superior
    p.x1=-7;p.y1=5;
    p.x2=7;p.y2=5;
    paredes.push_back(p);

    fondo_izq.r=0;
    fondo_izq.x1=-7;fondo_izq.y1=-5;
    fondo_izq.x2=-7;fondo_izq.y2=5;

    fondo_dcho.r=0;
    fondo_dcho.x1=7;fondo_dcho.y1=-5;
    fondo_dcho.x2=7;fondo_dcho.y2=5;

    //a la izq
    jugador1.g=0;
    jugador1.x1=-6;jugador1.y1=-1;
    jugador1.x2=-6;jugador1.y2=1;

    //a la dcha
    jugador2.g=0;
    jugador2.x1=6;jugador2.y1=-1;
    jugador2.x2=6;jugador2.y2=1;
}

	
	
	

