//El autor de este codigo es Sara Rodriguez Fernandez
// Mundo.cpp: implementation of the CMundo class.
//
#include <fstream>
#include "MundoServidor.h"
#include "glut.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/mman.h>
#include <pthread.h>


void CMundo::RecibeComandosJugador(){
    while(1){
        usleep(10);
        
        char buf[10];
        //read(fdteclas, buf, sizeof(buf));
        if(sock_cs.Receive(buf, 1) < 1){
            perror("Error lectura tecla");
            exit(1);
        }
        
        unsigned char key;
        sscanf(buf, "%c", &key);
        
        switch(key)
        {
        case 's': jugador1.velocidad.y=-4; break;
        case 'w': jugador1.velocidad.y=4; break;
        case 'l': jugador2.velocidad.y=-4; break;
        case 'o': jugador2.velocidad.y=4; break;
      
        }
    }
}

void* hilo_comandos(void* d){
    CMundo* p=(CMundo*)d;
    p->RecibeComandosJugador();
}

CMundo::CMundo()
{
    Init();
}

CMundo::~CMundo()
{
   close(fd); //cerramos la tubería con logger
   unlink("/tmp/FIFO");
    /*close(fdsc);
close(fdteclas);*/
    pthread_join(thid1, NULL);
    sock_cs.Close();
    exit(0);
}

void CMundo::InitGL()
{
    //Habilitamos las luces, la renderizacion y el color de los materiales
    glEnable(GL_LIGHT0);
    glEnable(GL_LIGHTING);
    glEnable(GL_DEPTH_TEST);
    glEnable(GL_COLOR_MATERIAL);
    
    glMatrixMode(GL_PROJECTION);
    gluPerspective( 40.0, 800/600.0f, 0.1, 150);
}

void print(char *mensaje, int x, int y, float r, float g, float b)
{
    glDisable (GL_LIGHTING);

    glMatrixMode(GL_TEXTURE);
    glPushMatrix();
    glLoadIdentity();

    glMatrixMode(GL_PROJECTION);
    glPushMatrix();
    glLoadIdentity();
    gluOrtho2D(0, glutGet(GLUT_WINDOW_WIDTH), 0, glutGet(GLUT_WINDOW_HEIGHT) );

    glMatrixMode(GL_MODELVIEW);
    glPushMatrix();
    glLoadIdentity();

    glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    glDisable(GL_DEPTH_TEST);
    glDisable(GL_BLEND);
    glColor3f(r,g,b);
    glRasterPos3f(x, glutGet(GLUT_WINDOW_HEIGHT)-18-y, 0);
    int len = strlen (mensaje );
    for (int i = 0; i < len; i++) 
        glutBitmapCharacter (GLUT_BITMAP_HELVETICA_18, mensaje[i] );
        
    glMatrixMode(GL_TEXTURE);
    glPopMatrix();

    glMatrixMode(GL_PROJECTION);
    glPopMatrix();

    glMatrixMode(GL_MODELVIEW);
    glPopMatrix();

    glEnable( GL_DEPTH_TEST );
}
void CMundo::OnDraw()
{
    //Borrado de la pantalla    
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    //Para definir el punto de vista
    glMatrixMode(GL_MODELVIEW); 
    glLoadIdentity();
    
    gluLookAt(0.0, 0, 17,  // posicion del ojo
        0.0, 0.0, 0.0,      // hacia que punto mira  (0,0,0) 
        0.0, 1.0, 0.0);      // definimos hacia arriba (eje Y)    

    /////////////////
    ///////////
    //      AQUI EMPIEZA MI DIBUJO
    char cad[100];
    sprintf(cad,"Jugador1: %d",puntos1);
    print(cad,10,0,1,1,1);
    sprintf(cad,"Jugador2: %d",puntos2);
    print(cad,650,0,1,1,1);
    
    for(int i=0;i<paredes.size();i++)
        paredes[i].Dibuja();

    fondo_izq.Dibuja();
    fondo_dcho.Dibuja();
    jugador1.Dibuja();
    jugador2.Dibuja();
    esfera.Dibuja();

    /////////////////
    ///////////
    //      AQUI TERMINA MI DIBUJO
    ////////////////////////////

    //Al final, cambiar el buffer
    glutSwapBuffers();
}

void CMundo::OnTimer(int value)
{   
    
        jugador1.Mueve(0.025f);
        jugador2.Mueve(0.025f);
        esfera.Mueve(0.025f);
        for(int i=0;i<paredes.size();i++)
        {
            paredes[i].Rebota(esfera);
            paredes[i].Rebota(jugador1);
            paredes[i].Rebota(jugador2);
        }

        jugador1.Rebota(esfera);
        jugador2.Rebota(esfera);
        if(fondo_izq.Rebota(esfera))
        {
            esfera.centro.x=0;
            esfera.centro.y=rand()/(float)RAND_MAX;
            esfera.velocidad.x=2+2*rand()/(float)RAND_MAX;
            esfera.velocidad.y=2+2*rand()/(float)RAND_MAX;
            if(esfera.radio>0.15f) esfera.radio-=0.05f;
            puntos2++;
            char buf[]={'2',(char)(puntos2 + 48)};
	    write(fd, buf, 2);
        }

        if(fondo_dcho.Rebota(esfera))
        {
            esfera.centro.x=0;
            esfera.centro.y=rand()/(float)RAND_MAX;
            esfera.velocidad.x=-2-2*rand()/(float)RAND_MAX;
            esfera.velocidad.y=-2-2*rand()/(float)RAND_MAX;
            if(esfera.radio>0.15f) esfera.radio-=0.05f;
            puntos1++;
            char buf[]={'1', (char)(puntos1 + 48)};
	    write(fd, buf, 2);
        }

    
    
    //Datos servidor-cliente
    char buf_sc[200];
    sprintf(buf_sc,"%f %f %f %f %f %f %f %f %f %f %f %d %d",                                             esfera.centro.x,esfera.centro.y,esfera.radio,jugador1.x1,jugador1.y1,jugador1.x2,jugador1.y2,jugador2.x1,jugador2.y1,jugador2.x2,jugador2.y2,puntos1,puntos2);
        
        /*int err=write(fdsc,buf_sc,sizeof(buf_sc));
	if (err<sizeof(buf_sc))
	{
	perror("Error al escribir fifo s-c");
	
	}*/
    int errsock=sock_cs.Send(buf_sc, sizeof(buf_sc));
    if (errsock < sizeof(buf_sc)){
        perror("Error al escribir fifo s-c");
        exit(1);
    }   
}

void CMundo::OnKeyboardDown(unsigned char key, int x, int y){}

void CMundo::Init()
{
    //Socket CS
    int initerv=sock_aux.InitServer("127.0.0.1", 25565);
    if (initerv< 0){
        perror("Error al iniciar servidor");
        exit(1);
    }
    sock_cs=sock_aux.Accept();
    
    char nombre[200];
    int errname=sock_cs.Receive(nombre, sizeof(nombre));
    if (errname < 0){
        perror("Error recepcion nombre");
        exit(1);
    }
    else{
        write(1, "Nombre: ", strlen("Nombre: "));
        write(1, nombre, strlen(nombre));
    }
    
    //Thread comandos
    int errorthread=pthread_create(&thid1, NULL, hilo_comandos, this);
    if (errorthread != 0){
        perror("Error al crear thread");
        exit(1);
    }

    //fifo logger
fd = open ("/tmp/FIFO", O_WRONLY);
if (fd<0){
perror ("error al abrir fifo logger");
exit(1);
}
    
/*fifo servidor-cliente (coordenadas)
fdsc= open ("/tmp/FIFO_SC", O_WRONLY);
if (fdsc<0){
perror ("error al abrir fifo servidor-cliente");
exit(1);
}

//fifo envio teclas
fdteclas= open ("/tmp/FIFO_TECLAS",O_RDONLY);
if (fdteclas<0){
perror ("error al abrir fifo teclas");
exit(1);
}*/

    Plano p;
//pared inferior
    p.x1=-7;p.y1=-5;
    p.x2=7;p.y2=-5;
    paredes.push_back(p);

//superior
    p.x1=-7;p.y1=5;
    p.x2=7;p.y2=5;
    paredes.push_back(p);

    fondo_izq.r=0;
    fondo_izq.x1=-7;fondo_izq.y1=-5;
    fondo_izq.x2=-7;fondo_izq.y2=5;

    fondo_dcho.r=0;
    fondo_dcho.x1=7;fondo_dcho.y1=-5;
    fondo_dcho.x2=7;fondo_dcho.y2=5;

    //a la izq
    jugador1.g=0;
    jugador1.x1=-6;jugador1.y1=-1;
    jugador1.x2=-6;jugador1.y2=1;

    //a la dcha
    jugador2.g=0;
    jugador2.x1=6;jugador2.y1=-1;
    jugador2.x2=6;jugador2.y2=1;
}

	
	
	

